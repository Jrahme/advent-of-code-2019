(ns secure-container.core
  (:gen-class))

(def pw-range (range 124075 (inc 580769)))

(defn lower-filter [part]
  (apply <= part))

;; pass each number through this mofo
(defn number-filter
  [i-vec]
  (let [window-prt (partition 2 1 i-vec)
        freq (frequencies i-vec)]
    (and 
      (every? lower-filter window-prt)
      (some #(= 2 %1) (vals freq)))))

(defn digits [n]
  (->> n str (map (comp read-string str))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (count (filter number-filter (map digits pw-range)))) 
  ;;(println (count (filter number-filter (map digits [112233 123444 111122]))))
  )
