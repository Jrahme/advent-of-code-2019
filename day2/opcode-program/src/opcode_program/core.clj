(ns opcode-program.core
  (:gen-class))
(def initial-program[1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,2,23,9,27,1,5,27,31,1,9,31,35,1,35,10,39,2,13,39,43,1,43,9,47,1,47,9,51,1,6,51,55,1,13,55,59,1,59,13,63,1,13,63,67,1,6,67,71,1,71,13,75,2,10,75,79,1,13,79,83,1,83,10,87,2,9,87,91,1,6,91,95,1,9,95,99,2,99,10,103,1,103,5,107,2,6,107,111,1,111,6,115,1,9,115,119,1,9,119,123,2,10,123,127,1,127,5,131,2,6,131,135,1,135,5,139,1,9,139,143,2,143,13,147,1,9,147,151,1,151,2,155,1,9,155,0,99,2,0,14,0])

(def program (atom nil))

(declare operate)

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (reset! program initial-program) 
  (loop [noun 0
         verb 0]
    (reset! program initial-program)
    (reset! program (assoc @program 1 noun))
    (reset! program (assoc @program 2 verb))
  (operate)
  (if (not (= (first @program) 19690720))
    (recur (if (= verb 99) (inc noun) noun)
            (if (= verb 99) 0 (inc verb)))
    (println (+ (* 100 noun) verb))
    )))

(defn- execute-
  [op v1 v2 _]
  (cond
    (= op 1) (+ (nth @program v1) (nth @program v2))
    (= op 2) (* (nth @program v1) (nth @program v2))
    ;; this could be a problem?
    (= op 99) @program
    :else (throw ( Exception. "Invalid op code"))))

(defn- execute
  [op v1 v2 pos]
  (update @program pos (partial execute- op v1 v2)))

;; just need to fix the partitioning scheme and how 99 is handled
(defn- operate
  []
  (loop [idx 0]
    (let [params (nth (partition 4 4 [nil nil nil nil] @program) idx)]
      (when (not (= (first params) 99))
        (reset! program (apply execute params)) 
        (recur (inc idx)))
      )))
