(ns fuel-calc.core
  (:gen-class))

(declare amass-fuel parse-weights calc-fuel)

(defn -main
  [& args]
  (with-open [weight-rdr (clojure.java.io/reader "module-weights")]
    (amass-fuel (parse-weights (line-seq weight-rdr)))))

(defn- parse-weights
  [weights]
  (map #(Integer. %1) (filter #(not (empty? %1)) weights)))

(defn- calc-fuel
  [w]
  {:pre [(integer? w)]}
  (loop [acc 0
         weight w]
    (let [calc (- (int (with-precision 10 :rounding FLOOR (/ weight 3))) 2)]
      (if (>= calc 0) 
        (recur (+ acc calc) calc)
        acc))))

(defn amass-fuel
  [weights]
  (reduce + (map calc-fuel weights)))
